#!/usr/bin/python3.8

# used to spin the mouse wheel in 'Minecraft' to help place random block.
# Spins the mouse wheel and so spins the toolbar in 'Minecraft' while you press the mouse button.

import random

from pynput.keyboard import Key, Listener
from pynput.mouse import Controller

mouse = Controller()


def key_check(key):
	try:
		if key.char == 'y':
			# this needs to be called till the esc key is pressed
			# This section is currently wrong
			i = 1
			while i != 0:
				scroll_wheel()
				if on_release(key) == 0:
					i = 0


		elif key.char == Key.esc:
			mouse.scroll(dx=0, dy=0)
			print('mouse = 0')
		else:
			mouse.scroll(dx=0, dy=0)
			print('key_check - else')
	except AttributeError:
		print('Key_check - finally')

# def key_check(key):
# 	try:
# 		if key.char == 'y':
# 			key_state = 1
# 			scroll_wheel_loop(key_state)
# 		else:
# 			print('key_check - else')
# 	except AttributeError:
# 		print('Key_check - finally')


# def scroll_wheel_loop(key_state):
# 	while key_state == 1:
# 		scroll_wheel()


def scroll_wheel():
	random_spin = random.randrange(1, 10)
	mouse.scroll(dx=0, dy=random_spin)


def on_release(key):
	if key == Key.esc:
		#key_check(key)
		return 0

# mouse.release(Button.right)
# mouse.release(Button.left)
# Stop listener
# return False
# else:
# pressed_key = key_check(key)

print('This program is ment to be used with Minecraft Java on a Linux based computer \nIt will allow you to place random blocks from your toolbar when right clicking the mouse.')
print()
print('Press the "y" to start the scrolling of the mouse wheel')
print('Press the "Esc" to stop the scrolling')

with Listener(
		on_press=key_check, on_release=on_release) as listener:
	listener.join()
